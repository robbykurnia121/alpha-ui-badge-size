const plugin = require('tailwindcss/plugin');

const badgeSize = plugin(
  function ({ addUtilities, theme, variants, e }) {
    const values = theme('badgeSize');

    addUtilities(
      [
        Object.entries(values).map(([key, value]) => {
          return {
            [`.${e(`badge-size-${key}`)}`]: {
              'width': `${value}`,
              'height': `${value}`,
            },
          };
        }),
      ],
      variants('badgeSize')
    );
  },
  {
    theme: {
      badgeSize: {
        'xs': '12px',
        'sm': '14px',
        'md': '16px',
        'lg': '20px',
        'xl': '24px',
        '2xl': '24px',
      },
    },
    variants: {
      badgeSize: [''],
    },
  }
);

module.exports = badgeSize;